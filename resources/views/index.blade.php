<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>¡Bienvenido a la cancha!</title>

    <link href="http://fonts.googleapis.com/css?family=Roboto:700,400&subset=latin" rel="stylesheet" type="text/css">  
    <link rel="stylesheet" href="css/common.min.css">
    <link rel="stylesheet" href="css/okayNav.min.css">
    <link rel="stylesheet" href="css/estilos.css">
    
</head>
<body>

    <header id="header" class="okayNav-header">
        <a class="okayNav-header__logo" href="{{url('/')}}">
           <img src="dist/img/logo2.png">
        </a>

        <nav role="navigation" id="nav-main" class="okayNav">
            <ul>
                <li><a href="{{url('/')}}">Inicio</a></li>
                <li><a href="#">Horarios</a></li>
                <li><a href="{{url('/acceder')}}">Acceder</a></li>
                <li><a href="{{url('/registrate')}}">Registrate</a></li>
                <li><a href="#">Contáctanos</a></li>
              
            </ul>
        </nav>
    </header>

    <main>
            <h2></h2>
    <div class="content-all">
        <center>
        <div class="content-carrousel">
            <figure><img src="image/img1.jpg"></figure>
            <figure><img src="image/img2.jpg"></figure>
            <figure><img src="image/img3.jpg"></figure>
            <figure><img src="image/img4.jpg"></figure>
            <figure><img src="image/img5.jpg"></figure>
            <figure><img src="image/img6.jpg"></figure>
            <figure><img src="image/img7.jpg"></figure>
            <figure><img src="image/img8.jpg"></figure>
            <figure><img src="image/img9.jfif"></figure>
            <figure><img src="image/img10.jpg"></figure>
        </div>
        </center>
    </div>
    <footer class="main-footer">
        <div class="float-right d-none d-sm-block">
          <b>Version</b> 4.0.2-pre
        </div>
        <strong>Copyright &copy
               2020-2021 <a href=""></a>.</strong> All rights
    reserved.
  </footer>
    </main>
 

    <script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
    <script src="js/jquery.okayNav-min.js"></script>

    <script type="text/javascript">
        var navigation = $('#nav-main').okayNav()

    </script>

</body>
</html>
