@extends('master')
@section('logo')
<div class="image">
          <img src="dist/img/usuario-registrados.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Nombre Usuario</a>
        </div>
@endsection('logo')
@section('title')
Registro
@endsection('title')
@section('titlesec')


      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Registrarse como nuevo usuario</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
          <!--aqui inicia el registro de usuarios-->
<div class="card-body">
<form class="form-group" action="/users" method="POST">
      @csrf
      <div class="input-group mb-3">
      <input type="text" class="form-control" placeholder="Cédula" name="cedula" required>
      <div class="input-group-append">
      <div class="input-group-text">
        <span class="fas fa-user"></span>
      </div>
      </div>
      </div>
      <div class="input-group mb-3">
      <input type="text" class="form-control" placeholder="Nombre completo" name="user" required>
      <div class="input-group-append">
      <div class="input-group-text">
        <span class="fas fa-user"></span>
      </div>
      </div>
      </div>
      <div class="input-group mb-3">
      <input type="email" class="form-control" placeholder="Correo" name="email" required>
      <div class="input-group-append">
      <div class="input-group-text">
        <span class="fas fa-envelope"></span>
      </div>
      </div>
      </div>
      <div class="input-group mb-3">
      <input type="text" class="form-control" placeholder="Teléfono" name="phone" required>
      <div class="input-group-append">
      <div class="input-group-text">
        <span class="fas fa-phone"></span>
      </div>
      </div>
      </div>
      <div class="input-group mb-3">
      <input id="p1" type="password" class="form-control" placeholder="Contraseña" name="password" required>
      <div class="input-group-append">
      <div class="input-group-text">
        <span class="fas fa-lock"></span>
      </div>
      </div>
      </div>
      <div class="input-group mb-3">
      <input id="p2" type="password" class="form-control" placeholder="Confirmar contraseña" name="password2" required>
      <div class="input-group-append">
      <div class="input-group-text">
        <span class="fas fa-lock"></span>
      </div>
      </div>
      </div>
      <div class="row">
      <div class="col-8">
      <div class="icheck-primary">
        <input type="checkbox" id="agreeTerms" name="terms" required>
        <label for="agreeTerms">
        <p>Estoy de acuerdo con los  <a href="#">terminos y condiciones</a></p>
        </label>
      </div>
      </div>
      </div>
      <!-- /.col -->
      <div class="col-4">
      <button type="submit" class="btn btn-primary btn-block" style="width:130%">Registrarse</button>
      </div>
      <!-- /.col -->
      </div>
    </form>
          <!--aqui termina el registro de usuarios-->
        </div>
        <!-- /.card-body -->
        <!-- /.card-footer-->
      </div>

@endsection('titlesec')
@section('colapse')
@endsection('colapse')