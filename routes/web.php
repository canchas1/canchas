<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

Route::get('//{some}', function ($some) {
    return view('welcome'.$some);
});

*/
Route::resource('users', 'UserController');


Route::get('/acceder', function(){
    return view('login');
});

Route::get('/', function(){
    return view('index');
});

Route::get('/registrate', function(){
    return view('register');
});

Route::get('/canchas', function(){
    return view('canchas');
});

Route::get('/test', function(){
    return view('canchas');
});